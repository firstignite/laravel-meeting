<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent\Providers\Zoom\Concerns;

use FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Participant;
use FirstIgnite\LaravelMeetingNeoEloquent\Events\MeetingCanceled;
use FirstIgnite\LaravelMeetingNeoEloquent\Events\MeetingScheduled;
use FirstIgnite\LaravelMeetingNeoEloquent\Events\MeetingUpdated;
use FirstIgnite\LaravelMeetingNeoEloquent\Events\ParticipantAdded;
use FirstIgnite\LaravelMeetingNeoEloquent\Events\ParticipationCanceled;
use FirstIgnite\LaravelMeetingNeoEloquent\Exceptions\NoZoomRoomAvailable;
use FirstIgnite\LaravelMeetingNeoEloquent\MeetingAdder;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\MeetingRoom;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant as ParticipantPivot;

trait InteractsWithMeetings
{
    use InteractsWithZoom;
    use ProvidesSettings;

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\MeetingAdder $meetingAdder
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
     * @throws  \FirstIgnite\LaravelMeetingNeoEloquent\Exceptions\NoZoomRoomAvailable
     * @return void
     */
    public function scheduling(MeetingAdder $meetingAdder, Meeting $meeting): void
    {
        if ($this->shareRooms()) {
            $endTime = (clone $meetingAdder->startTime)->addMinutes($meetingAdder->duration);
            if (!$host = MeetingRoom::findAvailable($meetingAdder->startTime, $endTime)) {
                throw NoZoomRoomAvailable::create($meetingAdder);
            }
            $meetingAdder->hostedBy($host);
        }

        $zoomMeeting = $this->createZoomMeeting(
            $meetingAdder->host->uuid,
            $meetingAdder->topic,
            $meetingAdder->startTime,
            $meetingAdder->duration
        );
        // $meeting->withMetaAttributes([
        //     'zoom_id' => $zoomMeeting->id,
        // ]);
        // do NOT save on the MeetingAdder
        $meeting->zoom_id = strval($zoomMeeting->id);
        $meeting->save();
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
     * @return void
     */
    public function scheduled(Meeting $meeting): void
    {
        event(new MeetingScheduled($meeting));
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
     * @return void
     */
    public function updating(Meeting $meeting): void
    {
        if ($meeting->isDirty()) {
            if ($meeting->isDirty('start_time')) {
                if ($this->shareRooms() && $meeting->host->isBusyBetween($meeting->start_time, $meeting->end_time, $meeting)) {
                    //Search for another host if the current is not available for the new start_time and duration
                    if (!$host = MeetingRoom::findAvailable($meeting->start_time, $meeting->end_time)) {
                        throw NoZoomRoomAvailable::createFromModel($meeting);
                    }
                    $meeting->updateHost($host);
                }
            }

            //If the host was changed, create a new zoom meeting and delete the previous one.
            if ($meeting->isDirty('host_id')) {
                // $originalZoomMeetingId = $meeting->getMetaValue('zoom_id');
                $originalZoomMeetingId = $meeting->zoom_id;

                //Create a new zoom meeting hosted by the new user (room)
                $zoomMeeting = $this->createZoomMeeting(
                    $meeting->host->uuid,
                    $meeting->topic,
                    $meeting->start_time,
                    $meeting->duration
                );

                //Update the zoom id referente and register the participants in the new zoom meeting
                // $meeting->setMeta('zoom_id')->asInteger($zoomMeeting->id);
                $meeting->zoom_id = strval($zoomMeeting->id);
                $meeting->save();

                $meeting->participantsPivot->each(function ($participant) use ($meeting) {
                    $meeting->cancelParticipation($participant->participant);
                    $meeting->addParticipant($participant->participant);
                });

                //Delete the original zoom meeting
                $this->api->deleteMeeting($originalZoomMeetingId);
            } else {

                //Update the zoom meeting without changing user (room)
                $this->updateZoomMeeting(
                    $meeting->zoom_id,
                    $meeting->topic,
                    $meeting->start_time,
                    $meeting->duration
                );
            }
        }
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
     * @return void
     */
    public function updated(Meeting $meeting): void
    {
        event(new MeetingUpdated($meeting));
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
     * @return void
     */
    public function starting(Meeting $meeting): void
    {
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
     * @return void
     */
    public function started(Meeting $meeting): void
    {
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
     * @return void
     */
    public function ending(Meeting $meeting): void
    {
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
     * @return void
     */
    public function ended(Meeting $meeting): void
    {
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
     * @return void
     */
    public function canceling(Meeting $meeting): void
    {
        $this->api->deleteMeeting($meeting->zoom_id);
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
     * @return void
     */
    public function canceled(Meeting $meeting): void
    {
        event(new MeetingCanceled($meeting));
    }

    /**
     * Undocumented function
     *
    //  * @param \FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Participant $participant
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
     * @param string $uuid
     * @return void
     */
    public function participantAdding($participant, Meeting $meeting, string $uuid): void
    {
        $registrant = $this->api->addMeetingParticipant($meeting->zoom_id, [
            'email' => $participant->getParticipantEmailAddress(),
            'first_name' => $participant->getParticipantFirstName(),
            'last_name' => $participant->getParticipantLastName(),
        ]);

        // create participant relation and set properties on participant relation
        $participant_relation = $meeting->participants(get_class($participant))->save($participant);
        $participant_relation->registrantId = $registrant->registrantId;
        $participant_relation->joinUrl = $registrant->joinUrl;
        $participant_relation->email = $participant->getParticipantEmailAddress();
        $participant_relation->save();

        // $meeting->setMeta($uuid)->asObject($registrant);
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Participant $participant
     * @return void
     */
    public function participantAdded(Participant $participant): void
    {
        // set edge properties here, including registrantId (which should be uuid)
        // getting participant's meeting through uuid meta
        // if ($metaUuid = $participant->meeting->getMeta($participant->uuid)) {
        //     $participant->setMeta('registrantId')->asString($metaUuid->value->registrantId);
        //     $participant->setMeta('joinUrl')->asString($metaUuid->value->joinUrl);
        //     $participant->setMeta('email')->asString($participant->participant->getParticipantEmailAddress());

        //     $metaUuid->delete();
        // }

        event(new ParticipantAdded($participant));
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant $participant
     * @return void
     */
    public function participationCanceling(ParticipantPivot $participant): void
    {
        $registrant = [
            'id' => $participant->meta->registrantId,
            'email' => $participant->meta->email,
        ];

        $this->api->updateMeetingParticipantStatus($participant->meeting->meta->zoom_id, [
            'action' => 'cancel',
            'registrants' => [$registrant],
        ]);

        $participant->clearMetas();
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant $participant
     * @return void
     */
    public function participationCanceled(ParticipantPivot $participant): void
    {
        event(new ParticipationCanceled($participant));
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant $participant
     * @return void
     */
    public function participantJoining(ParticipantPivot $participant): void
    {
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant $participant
     * @return void
     */
    public function participantJoined(ParticipantPivot $participant): void
    {
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant $participant
     * @return void
     */
    public function participantLeaving(ParticipantPivot $participant): void
    {
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant $participant
     * @return void
     */
    public function participantLeft(ParticipantPivot $participant): void
    {
    }
}
