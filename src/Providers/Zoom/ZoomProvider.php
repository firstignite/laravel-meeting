<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent\Providers\Zoom;

use FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Participant;
use FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Provider;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting;

class ZoomProvider implements Provider
{
    use Concerns\InteractsWithMeetings;

    /**
     * @var Zoom
     */
    protected Sdk\Zoom $api;

    /**
     * Undocumented function
     *
     * @param Zoom $zoom
     */
    public function __construct(Sdk\Zoom $zoom)
    {
        $this->api = $zoom;
    }

    /**
     * Undocumented function
     *
     * @return string
     */
    public function getFacadeAccessor(): string
    {
        return 'zoom';
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
     * @return mixed
     */
    public function getPresenterAccess(Meeting $meeting)
    {
        if ($zoomMeetingId = $meeting->zoom_id) {
            return optional($this->api->meeting($zoomMeetingId))->startUrl;
        }
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Participant $participant
     * @return mixed
     */
    public function getParticipantAccess(Meeting $meeting, Participant $participant)
    {
        return optional($meeting->participant($participant))->meta->joinUrl;
    }
}
