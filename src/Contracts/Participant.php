<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent\Contracts;

use Carbon\Carbon;
use Vinelab\NeoEloquent\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Vinelab\NeoEloquent\Eloquent\Relations\BelongsToMany;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant as ParticipantPivot;

interface Participant
{
    /**
     * Get the MorphToMany Relation with the Meeting Model
     *
     * @return \Vinelab\NeoEloquent\Eloquent\Relations\BelongsToMany
     */
    public function meetings(): BelongsToMany;

    /**
     * Email Address of the participant
     *
     * @return string
     */
    public function getParticipantEmailAddress(): string;

    /**
     * First name of the participant
     *
     * @return string
     */
    public function getParticipantFirstName(): string;

    /**
     * Last name of the participant
     *
     * @return string
     */
    public function getParticipantLastName(): string;

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
    * @return \FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant
     */
    public function bookMeeting(Meeting $meeting): ParticipantPivot;

    /**
    * Undocumented function
    *
    * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
    * @return bool
    */
    public function cancelMeetingParticipation(Meeting $meeting): bool;

    /**
    * Undocumented function
    *
    * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
    * @return \FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant
    */
    public function joinMeeting(Meeting $meeting): ParticipantPivot;

    /**
    * Undocumented function
    *
    * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
    * @return \FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant
    */
    public function leaveMeeting(Meeting $meeting): ParticipantPivot;

    /**
     * Undocumented function
     *
     * @param \Vinelab\NeoEloquent\Eloquent\Builder $query
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting|null $except
     * @return \Vinelab\NeoEloquent\Eloquent\Builder
     */
    public function scopeAvailableBetween(Builder $query, Carbon $start, Carbon $end, ?Meeting $except = null): Builder;

    /**
     * Undocumented function
     *
     * @param \Vinelab\NeoEloquent\Eloquent\Builder $query
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting|null $except
     * @return \Vinelab\NeoEloquent\Eloquent\Builder
     */
    public function scopeBusyBetween(Builder $query, Carbon $start, Carbon $end, ?Meeting $except = null): Builder;

    /**
     * Undocumented function
    *
    * @param \Carbon\Carbon $start
    * @param \Carbon\Carbon $end
    * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting|null $except
    * @return bool
    */
    public function isAvailableBetween(Carbon $start, Carbon $end, ?Meeting $except = null): bool;

    /**
     * Undocumented function
     *
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting|null $except
     * @return bool
     */
    public function isBusyBetween(Carbon $start, Carbon $end, ?Meeting $except = null): bool;
}
