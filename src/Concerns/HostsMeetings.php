<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent\Concerns;

use Carbon\Carbon;
// use Illuminate\Database\Eloquent\Relations\MorphMany;
use Vinelab\NeoEloquent\Eloquent\Relations\BelongsToMany;
use Vinelab\NeoEloquent\Eloquent\Relations\MorphMany;
use FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Host;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Traits\VerifiesAvailability;

/**
 * Provides default implementation of Host contract.
 */
trait HostsMeetings
{
    use VerifiesAvailability;
    
    /**
     * Get the MorphMany Relation with the Meeting Model
     *
     * @return \Vinelab\NeoEloquent\Eloquent\Relations\BelongsToMany
     */
    public function meetings(): BelongsToMany
    {
        // return $this->morphMany('Meeting', 'HOSTED_BY');
        return $this->belongsToMany('Meeting', 'HAS_HOST')->with('scheduler', 'presenter');
        // return  $this->morphMany(Meeting::class, 'host')->with('scheduler', 'presenter');
    }

    /**
     * Undocumented function
     *
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting|null $except
     * @return \FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Host|null
     */
    public static function findAvailable(Carbon $start, Carbon $end, ?Meeting $except = null): ?Host
    {
        return static::availableBetween($start, $end, $except)->inRandomOrder()->first();
    }
}
