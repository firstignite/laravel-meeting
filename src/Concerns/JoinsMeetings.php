<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent\Concerns;

// use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Vinelab\NeoEloquent\Eloquent\Relations\MorphMany;
use Vinelab\NeoEloquent\Eloquent\Relations\HyperMorph;
use Vinelab\NeoEloquent\Eloquent\Relations\BelongsToMany;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Traits\VerifiesAvailability;

/**
 * Provides default implementation of Participant contract.
 */
trait JoinsMeetings
{
    use VerifiesAvailability;
    
    /**
     * Get the HyperMorph Relation with the Meeting Model
     *
     * @return \Vinelab\NeoEloquent\Eloquent\Relations\BelongsToMany
     */
    public function meetings(): BelongsToMany
    {
        // return $this->hyperMorph($morph, 'Participant', 'IS_PARTICIPANT', 'IN');
        return $this->belongsToMany('Meeting', 'HAS_PARTICIPANT');

        // return $this->morphToMany(Meeting::class, 'participant', 'meeting_participants')
        //     ->withPivot(['uuid', 'started_at', 'ended_at'])
        //     ->withTimestamps()
        //     ->with('scheduler', 'presenter', 'host');
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
     * @return \FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant
     */
    public function bookMeeting(Meeting $meeting): Participant
    {
        return $meeting->addParticipant($this);
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
     * @return bool
     */
    public function cancelMeetingParticipation(Meeting $meeting): bool
    {
        return $meeting->cancelParticipation($this);
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
     * @return \FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant
     */
    public function joinMeeting(Meeting $meeting): Participant
    {
        return $meeting->joinParticipant($this);
    }

    /**
    * Undocumented function
    *
    * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
    * @return \FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant
    */
    public function leaveMeeting(Meeting $meeting): Participant
    {
        return $meeting->leaveParticipant($this);
    }
}
