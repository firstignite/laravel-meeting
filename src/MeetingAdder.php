<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Host;
use FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Presenter;
use FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Provider;
use FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Scheduler;
use FirstIgnite\LaravelMeetingNeoEloquent\Exceptions\BusyForTheMeeting;

class MeetingAdder implements Arrayable
{

    /**
     * @var \Carbon\Carbon
     */
    public Carbon $startTime;

    /**
     * @var int
     */
    public int $duration;

    /**
     * @var string
     */
    public string $topic;

    /**
     * @var \FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Scheduler
     */
    public Scheduler $scheduler;

    /**
     * @var \FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Host
     */
    public Host $host;

    /**
     * @var \FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Presenter
     */
    public Presenter $presenter;

    /**
     * @var \FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Provider
     */
    public Provider $provider;


    /**
     * @var array
     */
    public array $metaAttributes = [];

    /**
     * Undocumented function
     *
     * @param string $topic
     * @return self
     */
    public function withTopic(string $topic): self
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param \Carbon\Carbon $startTime
     * @return self
     */
    public function startingAt(Carbon $startTime): self
    {
        $now = now();

        if ($startTime->lessThanOrEqualTo($now)) {
            //@todo exception startTime cannot be less than now
        }

        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param int $minutes
     * @return self
     */
    public function during(int $minutes): self
    {
        $this->duration = $minutes;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Scheduler $scheduler
     * @return self
     */
    public function scheduledBy(Scheduler $scheduler): self
    {
        $this->scheduler = $scheduler;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Host $host
     * @return self
     */
    public function hostedBy(Host $host): self
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Presenter $presenter
     * @return self
     */
    public function presentedBy(Presenter $presenter): self
    {
        $this->presenter = $presenter;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param string|null $provider
     * @return self
     */
    public function withProvider(?string $provider = null): self
    {
        $provider = $provider ? $provider : config('meeting.default');

        if (!config('meeting.providers.' . $provider)) {
            throw \FirstIgnite\LaravelMeetingNeoEloquent\Exceptions\InvalidProvider::create($provider);
        }

        $provider = resolve("laravel-meeting:{$provider}");

        $this->provider = $provider;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param array $metaAttributes
     * @return self
     */
    // public function withMetaAttributes(array $metaAttributes): self
    // {
    //     $this->metaAttributes = array_merge(
    //         $metaAttributes,
    //         $this->metaAttributes
    //     );

    //     return $this;
    // }

    /**
     * Undocumented function
     *
     * @param array $settings
     * @throws  \FirstIgnite\LaravelMeetingNeoEloquent\Exceptions\BusyForTheMeeting
     * @return void
     */
    protected function preventConcurrent(array $settings)
    {
        $endTime = (clone $this->startTime)->addMinutes($this->duration);

        foreach ($settings as $relation => $allowed) {
            if (
                !$allowed && isset($this->{$relation})
                && $this->{$relation}->isBusyBetween($this->startTime, $endTime)
            ) {
                throw BusyForTheMeeting::create($this, $relation);
            }
        }
    }

    /**
     * Undocumented function
     *
     * @return Models\Meeting
     */
    public function save(): Models\Meeting
    {
        $meeting = null;
        try {
            // $this->preventConcurrent(
            //     config('meeting.allow_concurrent_meetings', [])
            // );

            $meeting = new Models\Meeting([
                'uuid' => \Illuminate\Support\Str::uuid()->toString(),
                'topic' => $this->topic,
                'start_time' => $this->startTime,
                'duration' => strval($this->duration),
                'provider' => $this->provider->getFacadeAccessor(),
            ]);
            $meeting->save();

            $this->provider->scheduling($this, $meeting);

            // foreach ($this->metaAttributes as $key => $value) {
            //     $meeting->setMeta($key)->value($value);
            // }
            
            $meeting->scheduler(get_class($this->scheduler))->relate($this->scheduler);
            $meeting->presenter(get_class($this->presenter))->relate($this->presenter);
            $meeting->host(get_class($this->host))->relate($this->host);

            $this->provider->scheduled($meeting);

            return $meeting;
        } catch (\ErrorException $e) {
            if ($meeting != null) {
                $meeting->delete();
            }
            throw $e;
        }
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'topic' => $this->topic,
            'startTime' => $this->startTime->format('Y-m-d\TH:i:se'),
            'duration' => $this->duration,
            'provider' => $this->provider->getFacadeAccessor(),
            'scheduler' => $this->scheduler,
            'presenter' => $this->presenter,
            'host' => $this->host,
            // 'metaAttributes' => $this->metaAttributes,
        ];
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Support\Collection
     */
    public function toCollection(): \Illuminate\Support\Collection
    {
        return new Collection($this->toArray());
    }
}
