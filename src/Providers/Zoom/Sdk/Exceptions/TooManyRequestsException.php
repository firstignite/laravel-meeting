<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent\Providers\Zoom\Sdk\Exceptions;

use Exception;

class TooManyRequestsException extends Exception
{
    /**
     * Create a new exception instance.
     *
     * @param  array  $errors
     * @return void
     */
    public function __construct(object $response)
    {
        $this->message = $response->message;
    }
}
