<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent\Models\Traits;

use Illuminate\Support\Str;

trait UsesUuids
{
  // protected $neoLabel;

  /**
   * Boot function from Laravel.
   */
  protected static function boot()
  {
    parent::boot();
    static::creating(function ($model) {
      if (empty($model->{$model->getKeyName()})) {
        $model->uuid = Str::uuid()->toString();
      }
      // $this->neoLabel = class_basename(get_class($model));
    });
  }

  /**
   * Get the value indicating whether the IDs are incrementing.
   *
   * @return bool
   */
  public function getIncrementing()
  {
    return false;
  }

  /**
   * Get the auto-incrementing key type.
   *
   * @return string
   */
  public function getKeyType()
  {
    return 'string';
  }

  /**
   * Get the primary key for the model.
   *
   * @return string
   */
  public function getKeyName()
  {
    return 'uuid';
  }

  /**
   * Get Model Label name.
   *
   * @return string
   */
  // public function getLabel()
  // {
  //     return $this->neoLabel;
  // }
}
