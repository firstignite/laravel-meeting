<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent\Models;

use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquentModel;
use Vinelab\NeoEloquent\Eloquent\SoftDeletes;
use FirstIgnite\LaravelMeetingNeoEloquent\Concerns\HostsMeetings;
use FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Host;

class MeetingRoom extends NeoEloquentModel implements Host
{
    use SoftDeletes;
    use Traits\UsesUuids;
    use HostsMeetings;

    /**
    * The attributes that should be cast to native types.
    *
    * @var array
    */
    protected $casts = [
        'uuid' => 'string',
    ];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'name',
        'email',
        'type',
        'group',
    ];
}
