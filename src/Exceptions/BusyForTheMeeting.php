<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent\Exceptions;

use FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Participant;
use FirstIgnite\LaravelMeetingNeoEloquent\MeetingAdder;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting;

class BusyForTheMeeting extends \Exception
{

    /**
     * @var \FirstIgnite\LaravelMeetingNeoEloquent\MeetingAdder
     */
    protected MeetingAdder $meeting;

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\MeetingAdder $meeting
     * @param string $relation
     * @return self
     */
    public static function create(MeetingAdder $meeting, string $relation): self
    {
        return new static(
            'There `%s` is busy between `%s` and `%s` to be %s in the meeting with topic `%s`',
            $meeting,
            $relation
        );
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Participant $participant
     * @return self
     */
    public static function createForParticipant(Meeting $meeting, Participant $participant): self
    {
        $meetingAdder = (new MeetingAdder)
            ->scheduledBy($meeting->scheduler)
            ->presentedBy($meeting->presenter)
            ->hostedBy($meeting->host)
            ->withTopic($meeting->topic)
            ->startingAt($meeting->start_time)
            ->during($meeting->duration);

        $meetingAdder->participant = $participant;

        return static::create($meetingAdder, 'participant');
    }

    /**
     * Create a new instance of NoZoomRoomAvailable exception
     *
     * @param string $message
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\MeetingAdder $meeting
     * @param string $relation
     */
    public function __construct(string $message, MeetingAdder $meeting, string $relation)
    {
        $actor = get_class($meeting->{$relation}).':'.$meeting->{$relation}->id;

        $this->meeting = $meeting;
        $this->message = sprintf(
            $message,
            $actor,
            $meeting->startTime->format('Y-m-d H:i:se'),
            (clone $meeting->startTime)->addMinutes($meeting->duration)->format('Y-m-d H:i:se'),
            $relation,
            $meeting->topic
        );
    }

    /**
     * Get the meeting that regeneted the exception
     *
     * @return \FirstIgnite\LaravelMeetingNeoEloquent\MeetingAdder
     */
    public function getMeeting(): MeetingAdder
    {
        return $this->meeting;
    }
}
