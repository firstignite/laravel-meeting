<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent\Events;

use Illuminate\Queue\SerializesModels;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant;

class ParticipationCanceled
{
    use SerializesModels;

    public Participant $participant;

    /**
     * Create a new event instance.
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant $participant
     */
    public function __construct(Participant $participant)
    {
        $this->participant = $participant;
    }
}
